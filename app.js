const express = require('express');
const app = express();

const comments = {
    Joker: ["Buena Pelicula", "No me gustó"],
    It: ["Buena Pelicula", "No me gustó"]
}

app.get('/movies/:id', (req, res) => {
    let movieId = req.params.id;
    const movieComments = comments["Joker"];
    res.jsonp(movieComments);
})

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on port ${port}...`));

